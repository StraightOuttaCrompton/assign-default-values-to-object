import assignDefaultValuesToObject, { RecursiveRequired } from ".";

interface IObject {
    requiredProp: string;
    optionalProp1?: string;
    optionalProp2?: string;
}

const object: IObject = {
    requiredProp: "objectRequiredProp",
    optionalProp1: "objectOptionalProp1"
};

const defaultObject: IObject = {
    requiredProp: "defaultObjectRequiredProp",
    optionalProp1: "defaultObjectOptionalProp1",
    optionalProp2: "defaultObjectOptionalProp2"
};

const parsedObject = assignDefaultValuesToObject(defaultObject, object) as RecursiveRequired<
    IObject
>;

console.log(parsedObject.requiredProp)
// objectRequiredProp

console.log(parsedObject.optionalProp1)
// objectOptionalProp1

console.log(parsedObject.optionalProp2)
// defaultObjectOptionalProp2
