// https://stackoverflow.com/questions/47914536/use-partial-in-nested-property-with-typescript
// https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-8.html#example-6

export type RecursiveRequired<T> = {
    [P in keyof T]-?: RecursiveRequired<T[P]>;
};
