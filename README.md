# Assign default values to object

An [npm module](https://www.npmjs.com/package/aws-cdk-static-site) exporting a function to assign default values to an object.

## Installing

```sh
npm i assign-default-values-to-object
```

## Basic Usage

```ts
import assignDefaultValuesToObject, { RecursiveRequired } from "assign-default-values-to-object";

interface IObject {
    requiredProp: string;
    optionalProp1?: string;
    optionalProp2?: string;
}

const object: IObject = {
    requiredProp: "objectRequiredProp",
    optionalProp1: "objectOptionalProp1"
};

const defaultObject: IObject = {
    requiredProp: "defaultObjectRequiredProp",
    optionalProp1: "defaultObjectOptionalProp1",
    optionalProp2: "defaultObjectOptionalProp2"
};

const parsedObject = assignDefaultValuesToObject(defaultObject, object) as RecursiveRequired<
    IObject
>;

console.log(parsedObject.requiredProp)
// objectRequiredProp

console.log(parsedObject.optionalProp1)
// objectOptionalProp1

console.log(parsedObject.optionalProp2)
// defaultObjectOptionalProp2
```

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md).
