# 1.0.0 (2022-09-19)


### Bug Fixes

* added recursive required type and updated readme example usage ([d573a83](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/commit/d573a83108b3c020217bb7b6df51a446c65a903d))
* package.json & package-lock.json to reduce vulnerabilities ([d220c08](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/commit/d220c08bc75149304b56dfe1c367808e71f35aaa))


### Features

* initial commit ([e9f27d6](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/commit/e9f27d6d8cbbd97597963f7753e34f1a964e5aa5))

## [1.0.2](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/compare/v1.0.1...v1.0.2) (2020-04-04)


### Bug Fixes

* package.json & package-lock.json to reduce vulnerabilities ([3eb6a79](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/commit/3eb6a796b73972f9200d8dd1f0447171754bfa48))

## [1.0.1](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/compare/v1.0.0...v1.0.1) (2020-04-04)


### Bug Fixes

* added recursive required type and updated readme example usage ([f7a610c](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/commit/f7a610c736ee0d6162a29cba0f491272c128da0c))

# 1.0.0 (2020-02-16)


### Features

* initial commit ([0390563](https://gitlab.com/StraightOuttaCrompton/assign-default-values-to-object/commit/0390563ea4e27fb973859b99184cd4a1fd8f77a9))
